function doInput (qNumber, qText, qType, qVisibleLabel, qSize) {
	var lOut;

	if (qVisibleLabel === false) {
		lOut = '<div class="form-group"><label class="wb-inv" for="q' + qNumber + '">' + qText + '</label>';
	} else {
		lOut = '<div class="form-group"><label for="q' + qNumber + '">' + qText + '</label>';
	}

	if (qType === "input") {
		lOut = lOut + '<input id="q' + qNumber + '" class="form-control" type="text" size="' + qSize + '" />'
	} else if (qType === "textarea") {
		lOut = lOut + '<textarea id="q' + qNumber + '" class="form-control" rows="4" cols="' + qSize + '"></textarea>'
	}

	return lOut;

}

function doQuestion (qNumber, qText, qType)  {
	var lOut;

	if (qType !== "input" && qType !== "textarea" && qType !== "none" && qType !== "link") {
		qType = "input";
	}

	if (qType === 'none') {
		lOut = '<div class="form-group"><strong>' + qText + '</strong>';
	} else if (qType === 'link') {
		lOut = '<div class="form-group"><a href=' + qText + '>' + qText + '</a>';
	} else {
		lOut = doInput(qNumber, qText, qType, true, 80)
	}

	return lOut + '</div>';
};

function doTableQuestions(aData) {
	var lOut = '', lData;

	if (aData.table !== undefined) {
		lOut = "<table class=\"table table-striped table-hover\"><caption>" + aData.table.caption + "</caption>"
		lOut = lOut + "<thead><tr>"

		// table head
		for (th in aData.table.headings) {
			if (aData.table.headings.hasOwnProperty(th)) {
				lData = aData.table.headings[th]; // shortcut
				lOut = lOut + "<th scope=\"col\">" + (lData.title === undefined ? lData : lData.title)+ "</th>";
				//console.log("th is " + th);
				lData.size = (lData.size === undefined ? 30 : lData.size);
			}
		}
		lOut = lOut +"</tr></thead>";

		//now do rows
		lOut = lOut + "<tbody>"
		for (tr in aData.table.rows) {
			if (aData.table.rows.hasOwnProperty(tr)) {
				lOut = lOut + "<tr>";

				// now each cell in table
				for (td in aData.table.rows[tr]) {
					if (aData.table.rows[tr].hasOwnProperty(td)) {
						lData = aData.table.rows[tr][td]; // shortcut
						//console.log("td is " + td)

						lOut = lOut + "<td>"
						if (lData.type === undefined && lData.expand === undefined) {
							lOut = lOut + lData;
						} else if (lData.expand === undefined) {
							lOut = lOut + doInput(lData.id, lData.text, lData.type, false, ((aData.table.headings[td].size === undefined) ? 30 : aData.table.headings[td].size));
						} else {
							lOut = lOut + "<details><summary>" + lData.expand.summary + "</summary>" + lData.expand.detail + "</details>";
						}

						lOut = lOut + "</div></td>";
					}
				}

				lOut = lOut + "</tr>";
			}
		}

		lOut = lOut + "</table>";
	}



	return lOut;
}

function doQuestions(aData) {
	//console.log("doQuestions: " + JSON.stringify(aData));
	var lOut = '', lSubData;
	for (key in aData.questions) {
		if (aData.questions.hasOwnProperty(key)) {
			lSubData = aData.questions[key] // shortcut
			//console.log("key: " + key + " with type " + lSubData.type + " = " + lSubData);

			if (lSubData.type === "sub") {
				// create sub question
				//console.log(lSubData.questions);
				lOut = lOut + "<li><strong>" + lSubData.lead + "</strong><" + lSubData.listype + ">" + doQuestions(lSubData) + "</" + lSubData.listype + "></li>";
			} else {
				//shunt over to doQuestion
				lOut = lOut + "<li>" + doQuestion(key, lSubData.q, lSubData.type) + "</li>";
			}
		}
	}

	return lOut;
}

module.exports.register = function (Handlebars, options, params)  {
	Handlebars.registerHelper('exquestion', function (qNumber, qText, qType)  {
		return doQuestion(qNumber, qText, qType);
	});

	Handlebars.registerHelper('exquestions', function (aData, aLang)  {
		var lOut, lData;
		//console.log(aData)
		//console.log(aLang)

		if (aLang === "en") {
			lData = aData.en;
		} else {
			lData = aData.fr;
		}

		lOut = doQuestions(lData);
		lOut = lOut + doTableQuestions(lData);

		//console.log("lOut: " + lOut);

		return lOut;
	});

	Handlebars.registerHelper('exolparts', function (aData, aLang)  {
		var lOut = '', lSubData;
		for (key in aData.time.rows) {
			if (aData.time.rows.hasOwnProperty(key)) {
				//console.log("time rows: " + JSON.stringify(aData.time.rows[key]));
				lSubData = aData.time.rows[key] // shortcut

				lOut = lOut + "<li>" + lSubData[aLang] + "</li>";
			}
		}

		return lOut;
	});
};



