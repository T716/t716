<h2>New York - Consulate General of Canada</h2>
<div>Street Address: 1251 Avenue of the Americas, Concourse Level, New York, New York, U.S.A., 10020-1175<br />
Telephone: (212) 596-1759<br />
Fax: (212) 596-1666/1790<br />
Email: cngny@international.gc.ca<br />
Internet: www.newyork.gc.ca<br />
</div>