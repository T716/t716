<h2>New York - Consulate General of Canada</h2>
<dl><dt>Street Address: </dt><dd>1251 Avenue of the Americas, Concourse Level, New York, New York, U.S.A., 10020-1175
<dt>Telephone: </dt><dd>(212) 596-1759
<dt>Fax: </dt><dd>(212) 596-1666/1790</dd>
<dt>Email: </dt><dd>cngny@international.gc.ca</dd>
<dt>Internet: </dt><dd>www.newyork.gc.ca</dd>