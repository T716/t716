<p>Canadian citizens must present one of the following valid Western Hemisphere Travel Initiative-compliant documents when entering the U.S. by land or water:

<ul>
<li>a passport, which must be valid until the date of expected departure; or</li>
<li>a NEXUS card;</li>
<li>a Free and Secure Trade (FAST) card;</li>
<li>an enhanced driver’s licence (EDL) or enhanced identification card (EIC) from a province or </li>territory where a U.S.-approved EDL/EIC program has been implemented; or
<li>a Secure Certificate of Indian Status.</li></ul></p>