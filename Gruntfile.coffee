#global module:false
module.exports = (grunt) ->

	# Default task.
	@registerTask(
		"default"
		"Default task, that runs the production build"
		[
			"dist"
		]
	)

	@registerTask(
		"dist"
		"Produces the production files"
		[
			"clean:dist"
			"doCopy"
			"css"
			"assemble"
		]
	)

	@registerTask(
		"doCopy"
		"Run the copy operations"
		[
			"copy:wetboew"
			"copy:assets"
			"copy:otherassets"
			"copy:media"
			"copy:tryitimgassets"
			"copy:tryitjsassets"
			"copy:moment"
		]
	)

	@registerTask(
		"init"
		"Only needed when the repo is first cloned"
		[
			"install-dependencies"
			"hub"
		]
	)

	@registerTask(
		"server"
		"Run the Connect web server for local repo"
		[
			"connect:server:keepalive"
		]
	)

	@registerTask(
		"deploy"
		"Build and deploy artifacts to wet-boew-dist"
		[
			"dist"
			"copy:deploy"
			"gh-pages:travis"
		]
	)

	@registerTask(
		"css"
		"Build and deploy artifacts to wet-boew-dist"
		[
			"sass:all"
			"sass:tryit"
			"autoprefixer"
			#"csslint:unmin"
			#"cssmin:v4"
		]
	)

	@initConfig
		pkg: @file.readJSON "package.json"
		jqueryVersion: @file.readJSON "lib/jquery/bower.json"
		jqueryOldIEVersion: @file.readJSON "lib/jquery-oldIE/bower.json"
		banner: "/*!\n * Web Experience Toolkit (WET) / Boîte à outils de l'expérience Web (BOEW)\n * wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html\n" +
				" * <%= pkg.version %> - " + "<%= grunt.template.today('yyyy-mm-dd') %>\n *\n */"

		assemble:
			options:
				prettify:
					indent: 2
				marked:
					sanitize: false
				production: false
				data: [
					"lib/wet-boew/site/data/**/*.{yml,json}"
					"site/data/**/*.{yml,json}"
				]
				helpers: [
					"lib/wet-boew/site/helpers/helper-*.js"
					"site/helpers/helper-*.js"
				]
				partials: [
					"lib/wet-boew/site/includes/**/*.hbs"
					"site/includes/**/*.hbs"
				]
				layoutdir: "site/layouts"
				layout: "default.hbs"
				environment:
					suffix: ".min"
					jqueryVersion: "<%= jqueryVersion.version %>"
					jqueryOldIEVersion: "<%= jqueryOldIEVersion.version %>"

			guides:
				options:
					assets: "dist"
				files: [
						#site
						expand: true
						cwd: "site/pages"
						src: [
							"**/*.hbs",
							"!index.hbs"
						]
						dest: "dist"
				]

			splash:
				options:
					layout: "splashpage.hbs"
					assets: "dist"
				cwd: "site/pages"
				src: [
					"index.hbs"
				]
				dest: "dist"
				expand: true

		copy:
			moment:
				expand: true
				cwd: "lib/moment"
				src: "moment.js"
				dest: "dist/js"
			wetboew:
				expand: true
				cwd: "lib/wet-boew/dist"
				src: [
					"**/*.*"
					"!demos/**/*.*"
					"!unmin/**/*.*"
				]
				dest: "dist"
			assets:
				expand: true
				cwd: "site/pages"
				src: [
					"**/images/*.*"
					"**/js/*.*"
					"**/js/*/*.*"
				]
				dest: "dist"
			otherassets:
				expand: true
				cwd: "site/data/Others"
				src: "**/*.*"
				dest: "dist/Others"
			media:
				expand: true
				cwd: "site/pages/media"
				src: "**/*.*"
				dest: "dist/media"								
			tryitimgassets:
				expand: true
				cwd: "site/data/Scenarios"
				src: [
					"**/images/*.*"
				]
				dest: "dist/images"
				rename: `function(dest, src) {
					var lOut;
					lOut = dest + "/" + src.replace(/\/images\//ig, "/");
					return lOut;}`
			tryitjsassets:
				expand: true
				cwd: "site/data/Scenarios"
				src: [
					"**/js/*.*"
				]
				dest: "dist/js"
				rename: `function(dest, src) {
					var lOut;
					lOut = dest + "/" + src.replace(/\/js\//ig, "/");
					return lOut;}`					
			deploy:
				src: [
					"*.txt"
					"README.md"
				]
				dest: "dist/images/tryit"
				expand: true
		# Compiles the Sass files
		sass:
			all:
				files: [
					expand: true
					cwd: "site/pages"
					src: [
						"**/*.scss"
					]
					dest: "dist"
					ext: ".css"
				]
			tryit:
				files: [
					expand: true
					cwd: "site/data/Scenarios"
					src: [
						"**/*.scss"
					]
					dest: "dist/css/tryit"
					ext: ".css"
				]

		autoprefixer:
			# Only vendor prefixing and no IE8
			modern:
				options:
					browsers: [
						"last 2 versions"
						"android >= 2.3"
						"bb >= 7"
						"ff >= 17"
						"ie > 8"
						"ios 5"
						"opera 12.1"
					]
				cwd: "dist"
				src: [
					"**/*.css"
					"!ie8*.css"
				]
				dest: "dist"
				expand: true

			# Needs both IE8 and vendor prefixing
			mixed:
				options:
					browsers: [
						"last 2 versions"
						"android >= 2.3"
						"bb >= 7"
						"ff >= 17"
						"ie >= 8"
						"ios 5"
						"opera 12.1"
					]
				files: [
					cwd: "dist"
					src: [
						"**/*.css"
						"!**/*.min.css"
					]
					dest: "dist"
					expand: true
					flatten: true
				]

			# Only IE8 support
			oldIE:
				options:
					browsers: [
						"ie 8"
					]
				cwd: "dist"
				src: [
					"ie8*.css"
				]
				dest: "dist"
				expand: true
				flatten: true

		csslint:
			options:
				"adjoining-classes": false
				"box-model": false
				"box-sizing": false
				"compatible-vendor-prefixes": false
				"duplicate-background-images": false
				"duplicate-properties": false
				# Can be turned off after https://github.com/dimsemenov/Magnific-Popup/pull/303 lands
				"empty-rules": false
				"fallback-colors": false
				"floats": false
				"font-sizes": false
				"gradients": false
				"headings": false
				"ids": false
				"important": false
				# Need due to use of "\9" hacks for oldIE
				"known-properties": false
				"outline-none": false
				"overqualified-elements": false
				"qualified-headings": false
				"regex-selectors": false
				# Some Bootstrap mixins end up listing all the longhand properties
				"shorthand": false
				"text-indent": false
				"unique-headings": false
				"universal-selector": false
				"unqualified-attributes": false
				# Zeros are output by some of the Bootstrap mixins, but shouldn't be used in our code
				"zero-units": false

		cssmin:
			options:
				banner: ""
			v4:
				options:
					banner: ""
				expand: true
				cwd: "dist"
				src: [
					"**/*.css"
				]
				dest: "dist"
				ext: ".min.css"
		clean:
			dist: 
				expand: true
				src: [
					"!dist/.git"
					"dist/**/*"
				]
			lib: ["lib"]
			non_mincss:
				expand: true
				src: [
					"dist/**/*.css"
					"!dist/**/*.min.css"
				]
			jsUncompressed: ["dist/js/**/*.js", "!dist/js/**/*<%= environment.suffix %>.js"]

		hub:
			"wet-boew":
				src: [
					"lib/wet-boew/Gruntfile.coffee"
				]
				tasks: [
					"build"
					"minify"
					"assets-min"
					"pages:ajax"
					"pages:min"
				]

		"install-dependencies":
			options:
				cwd: "lib/wet-boew"
				failOnError: false
				isDevelopment: true

		"gh-pages":
			options:
				base: "dist"

			travis:
				options:
					repo: "https://" + process.env.GH_TOKEN + "@bitbuket.org/t710/t710.bitbucket.org.git"
					message: "Travis build " + process.env.TRAVIS_BUILD_NUMBER
					silent: false
				src: [
					"**/*.*"
				]

		connect:
			options:
				port: 8000

			server:
				options:
					base: "dist"
					middleware: (connect, options, middlewares) ->
						middlewares.unshit(connect.compress(
							filter: (req, res) ->
								/json|text|javascript|dart|image\/svg\+xml|application\/x-font-ttf|application\/vnd\.ms-opentype|application\/vnd\.ms-fontobject/.test(res.getHeader('Content-Type'))
						))
						middlewares

	# These plugins provide necessary tasks.
	@loadNpmTasks "assemble"
	@loadNpmTasks "grunt-autoprefixer"
	@loadNpmTasks "grunt-contrib-clean"
	@loadNpmTasks "grunt-contrib-connect"
	@loadNpmTasks "grunt-contrib-copy"
	@loadNpmTasks "grunt-contrib-csslint"
	@loadNpmTasks "grunt-contrib-cssmin"
	@loadNpmTasks "grunt-gh-pages"
	@loadNpmTasks "grunt-hub"
	@loadNpmTasks "grunt-install-dependencies"
	@loadNpmTasks "grunt-sass"

	require( "time-grunt" )( grunt )
	@
