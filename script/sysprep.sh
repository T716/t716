#!/bin/bash

apt-get install curl
curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash -
apt-get install -y build-essential nodejs
npm update -g npm
npm install -g grunt-cli grunt bower